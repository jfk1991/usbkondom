(function() {
  "use strict";

  var isMobile = {
    Android: function() {
      return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
      return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
      return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
      return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
      return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
      return (
        isMobile.Android() ||
        isMobile.BlackBerry() ||
        isMobile.iOS() ||
        isMobile.Opera() ||
        isMobile.Windows()
      );
    }
  };

  var fullHeight = function() {
    if (!isMobile.any()) {
      $(".js-fullheight").css("height", $(window).height());
      $(window).resize(function() {
        $(".js-fullheight").css("height", $(window).height());
      });
    }
  };

  var sliderMain = function() {
    $("#fh5co-hero .flexslider").flexslider({
      animation: "fade",
      slideshowSpeed: 5000,
      directionNav: true,
      start: function() {
        setTimeout(function() {
          $(".slider-text").removeClass("animated fadeInUp");
          $(".flex-active-slide")
            .find(".slider-text")
            .addClass("animated fadeInUp");
        }, 500);
      },
      before: function() {
        setTimeout(function() {
          $(".slider-text").removeClass("animated fadeInUp");
          $(".flex-active-slide")
            .find(".slider-text")
            .addClass("animated fadeInUp");
        }, 500);
      }
    });

    $("#fh5co-hero .flexslider .slides > li").css("height", $(window).height());
    $(window).resize(function() {
      $("#fh5co-hero .flexslider .slides > li").css(
        "height",
        $(window).height()
      );
    });
  };

  var centerBlock = function() {
    $(".fh5co-section-with-image .fh5co-box").css(
      "margin-top",
      -($(".fh5co-section-with-image .fh5co-box").outerHeight() / 2)
    );
    $(window).resize(function() {
      $(".fh5co-section-with-image .fh5co-box").css(
        "margin-top",
        -($(".fh5co-section-with-image .fh5co-box").outerHeight() / 2)
      );
    });
  };

  var responseHeight = function() {
    setTimeout(function() {
      $(".js-responsive > .v-align").css(
        "height",
        $(".js-responsive > img").height()
      );
    }, 1);

    $(window).resize(function() {
      setTimeout(function() {
        $(".js-responsive > .v-align").css(
          "height",
          $(".js-responsive > img").height()
        );
      }, 1);
    });
  };

  var mobileMenuOutsideClick = function() {
    $(document).click(function(e) {
      var container = $("#fh5co-offcanvas, .js-fh5co-nav-toggle");
      if (!container.is(e.target) && container.has(e.target).length === 0) {
        if ($("body").hasClass("offcanvas-visible")) {
          $("body").removeClass("offcanvas-visible");
          $(".js-fh5co-nav-toggle").removeClass("active");
        }
      }
    });
  };

  var offcanvasMenu = function() {
    $("body").prepend('<div id="fh5co-offcanvas" />');
    $("#fh5co-offcanvas").prepend('<ul id="fh5co-side-links">');
    $("body").prepend(
      '<a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle"><i></i></a>'
    );
    $("#fh5co-offcanvas").append($("#fh5co-header nav").clone());
  };

  var burgerMenu = function() {
    $("body").on("click", ".js-fh5co-nav-toggle", function(event) {
      var $this = $(this);

      $("body").toggleClass("fh5co-overflow offcanvas-visible");
      $this.toggleClass("active");
      event.preventDefault();
    });

    $(window).resize(function() {
      if ($("body").hasClass("offcanvas-visible")) {
        $("body").removeClass("offcanvas-visible");
        $(".js-fh5co-nav-toggle").removeClass("active");
      }
    });

    $(window).scroll(function() {
      if ($("body").hasClass("offcanvas-visible")) {
        $("body").removeClass("offcanvas-visible");
        $(".js-fh5co-nav-toggle").removeClass("active");
      }
    });
  };

  var toggleBtnColor = function() {
    if ($("#fh5co-hero").length > 0) {
      $("#fh5co-hero").waypoint(
        function(direction) {
          if (direction === "down") {
            $(".fh5co-nav-toggle").addClass("dark");
          }
        },
        { offset: -$("#fh5co-hero").height() }
      );

      $("#fh5co-hero").waypoint(
        function(direction) {
          if (direction === "up") {
            $(".fh5co-nav-toggle").removeClass("dark");
          }
        },
        {
          offset: function() {
            return -$(this.element).height() + 0;
          }
        }
      );
    }
  };

  var contentWayPoint = function() {
    var i = 0;
    $(".animate-box").waypoint(
      function(direction) {
        if (direction === "down" && !$(this.element).hasClass("animated")) {
          i++;

          $(this.element).addClass("item-animate");
          setTimeout(function() {
            $("body .animate-box.item-animate").each(function(k) {
              var el = $(this);
              setTimeout(
                function() {
                  var effect = el.data("animate-effect");
                  if (effect === "fadeIn") {
                    el.addClass("fadeIn animated");
                  } else if (effect === "fadeInLeft") {
                    el.addClass("fadeInLeft animated");
                  } else if (effect === "fadeInRight") {
                    el.addClass("fadeInRight animated");
                  } else {
                    el.addClass("fadeInUp animated");
                  }

                  el.removeClass("item-animate");
                },
                k * 200,
                "easeInOutExpo"
              );
            });
          }, 100);
        }
      },
      { offset: "85%" }
    );
  };
  // Responsiveslides
  $(function() {
    $(".rslides").responsiveSlides({
      auto: true,
      speed: 1000,
      prevText: "Předchozí",
      nextText: "Další",
      pager: false,
      nav: false,
      pause: true
    });
  });

  // owlCarousel do not delete
  function owlInitialize() {
    if ($(window).width() < 768) {
      $(".owl-carousel-fullwidth").addClass("owl-carousel");
      $(".owl-carousel").owlCarousel({
        animateOut: "fadeOut",
        items: 1,
        loop: true,
        margin: 0,
        responsiveClass: true,
        nav: false,
        dots: true,
        smartSpeed: 500,
        autoHeight: true
      });
    } else {
      $(".owl-carousel").owlCarousel("destroy");
      $(".owl-carousel-fullwidth").removeClass("owl-carousel");
    }
  }
  $(document).ready(function(e) {
    owlInitialize();
  });
  $(window).resize(function() {
    owlInitialize();
  });
  // var testimonialCarousel = function() {
  //   var owl = $(".owl-carousel-fullwidth");
  //   owl.owlCarousel({
  //     animateOut: "fadeOut",
  //     items: 1,
  //     loop: true,
  //     margin: 0,
  //     responsiveClass: true,
  //     nav: false,
  //     dots: true,
  //     smartSpeed: 500,
  //     autoHeight: true
  //   });
  // };
  $(function() {
    fullHeight();
    sliderMain();
    centerBlock();
    responseHeight();
    mobileMenuOutsideClick();
    offcanvasMenu();
    burgerMenu();
    toggleBtnColor();
    contentWayPoint();
    // responsiveSlides();
    // testimonialCarousel();
  });
})();

//get values from calculator to mautic form

function getValuesFromCalc() {
  setTimeout(function() {
    getValuesFromCalc();
  }, 1000);
  if (typeof angular !== "undefined") {
    var scope = angular
      .element(document.querySelector("#calc .ng-scope"))
      .scope();
    if (
      typeof scope !== "undefined" &&
      typeof scope.calc !== "undefined" &&
      typeof scope.calc.fields !== "undefined" &&
      typeof scope.calc.fields[365666] !== "undefined"
    ) {
      $("#mauticform_input_antispaygadgetscompoptavka_produkt1").val(
        scope.calc.fields[365666].value
      );
    }
    if (
      typeof scope !== "undefined" &&
      typeof scope.calc !== "undefined" &&
      typeof scope.calc.fields !== "undefined" &&
      typeof scope.calc.fields[370869] !== "undefined"
    ) {
      $("#mauticform_input_antispaygadgetscompoptavka_pocet1").val(
        scope.calc.fields[370869].value
      );
    }
    if (
      typeof scope !== "undefined" &&
      typeof scope.calc !== "undefined" &&
      typeof scope.calc.fields !== "undefined" &&
      typeof scope.calc.fields[391150] !== "undefined"
    ) {
      $("#mauticform_input_antispaygadgetscompoptavka_pocet1_krytka").val(
        scope.calc.fields[391150].value
      );
    }
    if (
      typeof scope !== "undefined" &&
      typeof scope.calc !== "undefined" &&
      typeof scope.calc.fields !== "undefined" &&
      typeof scope.calc.fields[391150] !== "undefined"
    ) {
      $("#mauticform_input_antispaygadgetscompoptavka_pocet1_krytka").val(
        scope.calc.fields[391150].value
      );
    }
    if (
      typeof scope !== "undefined" &&
      typeof scope.calc !== "undefined" &&
      typeof scope.calc.fields !== "undefined" &&
      typeof scope.calc.fields[381623] !== "undefined"
    ) {
      $("#mauticform_input_antispaygadgetscompoptavka_pocet1_blocker").val(
        scope.calc.fields[381623].value
      );
    }
    if (
      typeof scope !== "undefined" &&
      typeof scope.calc !== "undefined" &&
      typeof scope.calc.fields !== "undefined" &&
      typeof scope.calc.fields[365670] !== "undefined"
    ) {
      $("#mauticform_input_antispaygadgetscompoptavka_produkt2").val(
        scope.calc.fields[365670].value
      );
    }
    if (
      typeof scope !== "undefined" &&
      typeof scope.calc !== "undefined" &&
      typeof scope.calc.fields !== "undefined" &&
      typeof scope.calc.fields[372386] !== "undefined"
    ) {
      $("#mauticform_input_antispaygadgetscompoptavka_pocet2").val(
        scope.calc.fields[372386].value
      );
    }
    if (
      typeof scope !== "undefined" &&
      typeof scope.calc !== "undefined" &&
      typeof scope.calc.fields !== "undefined" &&
      typeof scope.calc.fields[365671] !== "undefined"
    ) {
      $("#mauticform_input_antispaygadgetscompoptavka_produkt3").val(
        scope.calc.fields[365671].value
      );
    }
    if (
      typeof scope !== "undefined" &&
      typeof scope.calc !== "undefined" &&
      typeof scope.calc.fields !== "undefined" &&
      typeof scope.calc.fields[372387] !== "undefined"
    ) {
      $("#mauticform_input_antispaygadgetscompoptavka_pocet3").val(
        scope.calc.fields[372387].value
      );
    }
  }
}
getValuesFromCalc();

// TRANSLATIONS
function TransMod() {
  this.translate = function(lang, token) {
    return library[lang][token];
  };
  var library = new Array();

  library["cs"] = new Array();
  library["cs"]["translatable.navBar01"] = "Produkty";
  library["cs"]["translatable.navBar02"] = "100% bezpečí";
  library["cs"]["translatable.indexTitle"] =
    "AntiSpyGadgets – brnění pro vaši online bezpečnost";
  library["cs"]["translatable.indexIntro"] =
    "Produkty, které zajístí 100% ochranu vašeho soukromí i dat. Jedná se nejbezpečnější ochranu před zneužitím a to díky mechanickému zabezpečení.";
  library["cs"]["translatable.indexBtn"] = "Zjistit více";

  library["en"] = new Array();
  library["en"]["translatable.navBar01"] = "Products";
  library["en"]["translatable.navBar02"] = "100% security";
  library["en"]["translatable.indexTitle"] =
    "AntiSpyGadgets - shield for your online security";
  library["en"]["translatable.indexIntro"] =
    "Products that are interested in 100% protection of your privacy and data. This is the safest protection against misuse and mechanical security.";
  library["en"]["translatable.indexBtn"] = "Know more";
}
(function() {
  function InitStaticText(lang) {
    var langModule = new TransMod();
    $("#navBar01").html(
      langModule.translate(lang, $("#navBar01").attr("data-token"))
    );
    $("#navBar02").html(
      langModule.translate(lang, $("#navBar02").attr("data-token"))
    );
    $("#indexTitle").html(
      langModule.translate(lang, $("#indexTitle").attr("data-token"))
    );
    $("#indexIntro").html(
      langModule.translate(lang, $("#indexIntro").attr("data-token"))
    );
    $("#indexBtn").html(
      langModule.translate(lang, $("#indexBtn").attr("data-token"))
    );
  }

  InitStaticText("cs");
  $("#cs").click(function() {
    InitStaticText("cs");
  });
  $("#en").click(function() {
    InitStaticText("en");
  });
})();
// smoothScroll
$(document).on("click", 'a[href^="#"]', function(event) {
  event.preventDefault();

  $("html, body").animate(
    {
      scrollTop: $($.attr(this, "href")).offset().top
    },
    500
  );
});
// scrolly sticky menu
// var lastScrollTop = 0;
// $(window).scroll(function(event) {
//   var menuHeight = $("#fh5co-header").outerHeight();
//   if ($(document).scrollTop() > $(window).height() / 2) {
//     $("#fh5co-header").addClass("fixed");
//     $("body").css({ "padding-top": menuHeight });
//     var st = $(this).scrollTop();
//     if (st > lastScrollTop) {
//       $("#fh5co-header").removeClass("fixed-up");
//       $("#fh5co-header").addClass("fixed-down");
//     } else {
//       $("#fh5co-header").addClass("fixed-up");
//       $("#fh5co-header").removeClass("fixed-down");
//     }
//     lastScrollTop = st;
//   } else {
//     $("#fh5co-header").removeClass("fixed");
//     $("body").css({ "padding-top": 0 });
//   }
// });
// blog-post javascript
// function(){e("#navbar-nav-header .nav-link").on("click",function(t){var n,o=e(this).attr("href");n="#home"!==o?e(o).offset().top-50:0,e("html, body").animate({scrollTop:n},750),t.preventDefault()})};
// function(){e(n).scroll(function(){var t=e(n).scrollTop();return 150<t?(e("nav").removeClass("navbar-transparent"),e("body").addClass("not-on-top")):(e("body").removeClass("not-on-top"),e("nav").addClass("navbar-transparent")),!1})};
